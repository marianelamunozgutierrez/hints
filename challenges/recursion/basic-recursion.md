# Recursion: Basic Recursion

<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

In programming, repetition is a basic control structure to repeat the same code multiple times in execution. One way to implement repetition within code is through iteration by using **loops**. In contrast, **recursion** is an elegant and powerful alternative for performing repetitive tasks by having a function make one or more calls to itself during execution.

A recursive implementation always has two parts:<br>
&emsp;\- **base case**, the simplest and smallest instance of the problem that cannot be decomposed any further. It also works as a stop condition.<br>
&emsp;\- **recursive step**, which decomposes a larger instance of the problem into one or more simpler or smaller sub-problems that can be solved by recursive calls then recombines the results of those sub-problems to produce the overall solution.

Let's start by a simple counter implementation. We will explore how a recursive function handles the repetition.

```ruby
def iterative_counter(counter = 0)
  while counter < 10 # loop condition
    puts counter
    counter += 1
  end
end

def recursive_counter(counter = 0)
  return if counter > 10 # base case

  puts counter
  recursive_counter(counter + 1) # recursive call
end
```

The iterative implementation:<br>
&emsp;\- interrupts repetiton with a **loop condition**<br>
&emsp;\- uses a **while loop** to perform the repetition<br>

The recursive implementation:<br>
&emsp;\- interrupts repetition with a **base case**<br>
&emsp;\- **recursively calls** itself to perform the repetition

<br>

---

<br>

We have seen how to use recursion to perform a repetition.

Now, let's come back to the challenge. We need to add all positive integers up to a given number.

The iterative implementation is simply:

```ruby
def sum(number)
  sum = 0
  while number > 0
    sum += number
    number -= 1
  end
  sum
end
```

The formal expression for the iterative solution is:<br>
**`sum(number) = number + (number - 1) + (number - 2) + ... + 2 + 1`**

<br>

---

#### Think recursively

Recursion follows the divide-and-conquer approach. For now, simply be aware that we should handle each recursive step as a sub-problem. In the later challanges, we will expand on the divide-and-conquer approach.

First, describe the problem based on smaller sub-problems, then figure out when to stop dividing.

To find the sub-problems, it is a good practice to write it out plainly to uncover patterns that are often hidden:

**`sum(4) = 4 + 3 + 2 + 1`**<br>
**`sum(3) = 3 + 2 + 1`**

You can easily see that the solution of **sum(4)** contains the solution of **sum(3)**.

So, we can define:<br>
**`sum(4) = 4 + (3 + 2 + 1)`**<br>
Or, recursively:<br>
**`sum(4) = 4 + sum(3)`**<br>

<img src="images/recursion/basic-recursion/basic-recursion-1.png"  width="800"><br>

The formal expression for the recursive solution is:<br>
**`sum(number) = number + sum(number - 1) and sum(1) = 1`**

<b>`Hint:`&nbsp;Implement the solution according to the recursive pattern above.<br></b>

<br>
  
</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;How to trace recursive functions</summary><br>

Unlike in iterative implementations, understanding the repetition steps is not very straightforward in recursion.

A recursive algorithm often starts from the end and works backwards. This is the trickiest part of the recursive flow.<br>

Drawing a **recursion trace** helps us inspect each level of the recursive steps.

Here is a simple recursion trace of this challenge:<br>
<img src="images/recursion/basic-recursion/basic-recursion-2.png"  width="800"><br>

and a simple stack trace of this challenge:<br>
<img src="images/recursion/basic-recursion/basic-recursion-3.png"  width="800"><br>

As shown in the illustrations above, the execution starts by calling **sum(4)**. Then, it recurs until it arrives at the base case, **sum(1)**.
Finally, it computes the results by combining the sub-problems in backward order.

<b>`Hint:`&nbsp;Correct your implementation by using the tracing techniques above.<br></b>

</details>
<!-- Hint 2 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]:</b>&nbsp;Solution</summary><br>

```ruby
def sum(number)
  # base case:
  return 1 if number == 1
  # combine the current value with the recursive call result
  number + sum(number - 1)
end
```

</details>
<!-- Solution -->

